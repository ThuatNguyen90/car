export const environment = {
  production: true,
  SERVER_API_URL: 'prod.com',
  KEY_TOKEN: 'authWelToken',
  MAX_FILE_SIZE: 5 * 1024 * 1024,
  MAX_QUEUE_LIMIT: 5
};
