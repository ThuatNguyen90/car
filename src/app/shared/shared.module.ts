import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { HasAnyAuthorityDirective } from './directives/auth';
import { CookieModule } from 'ngx-cookie';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CommonModule, registerLocaleData} from '@angular/common';
import {
  CapitalizePipe,
  FilterPipe,
  KeysPipe,
  OrderByPipe,
  PureFilterPipe,
  TruncateCharactersPipe,
  TruncateWordsPipe,
  S3Image,
  FileSizePipe,
  FileNamePipe,
  FindLanguageFromKeyPipe
} from './pipe';
import { DataFilterPipe } from './pipe/datafilterpipe';
import { MediaPreviewDirective } from './directives/media-preview';
import { ItemCountComponent } from './components/item-count/item-count.component';
import { AlertComponent } from './components/alert/alert.component';
import { AlertErrorComponent } from './components/alert/alert-error.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../core/services/config.service';
import { TranslateModule } from '@ngx-translate/core';
import { ShowErrorsComponent } from './components/show-errors/show-errors.component';
import { DateTimeFormatPipe } from './pipe/date-time-format';
import {AlertModule} from 'ngx-bootstrap';
import {LanguageHelper} from './language/language.helper';
import {Title} from '@angular/platform-browser';
import {ModuleConfig} from '../core/services/config';
import {TranslateLanguageService} from '../core/services/translate-language.service';
import {LangMissingTranslationHandler} from './language/missing-translation';
import {TranslateComponent} from './language/translate.component';
import {locale} from 'moment';

export function translatePartialLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', `.json?buildTimestamp=${new Date().getTime()}`);
}

export function missingTranslationHandler(configService: ConfigService) {
  return new LangMissingTranslationHandler(configService);
}

@NgModule({
  imports: [
    CookieModule.forRoot(),
    AlertModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
  ],
  declarations: [
    HasAnyAuthorityDirective,
    CapitalizePipe,
    FilterPipe,
    KeysPipe,
    OrderByPipe,
    FileSizePipe,
    FileNamePipe,
    PureFilterPipe,
    TruncateCharactersPipe,
    TruncateWordsPipe,
    DataFilterPipe,
    DateTimeFormatPipe,
    S3Image,
    FindLanguageFromKeyPipe,
    MediaPreviewDirective,
    ItemCountComponent,
    AlertComponent,
    AlertErrorComponent,
    ShowErrorsComponent,
    TranslateComponent
  ],
  exports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    HasAnyAuthorityDirective,
    CapitalizePipe,
    FilterPipe,
    KeysPipe,
    FileNamePipe,
    OrderByPipe,
    FileSizePipe,
    PureFilterPipe,
    TruncateCharactersPipe,
    TruncateWordsPipe,
    DataFilterPipe,
    DateTimeFormatPipe,
    FindLanguageFromKeyPipe,
    S3Image,
    MediaPreviewDirective,
    ItemCountComponent,
    AlertComponent,
    AlertErrorComponent,
    TranslateModule,
    ShowErrorsComponent,
    TranslateComponent
  ],
  providers: [
    LanguageHelper,
    Title,
    {
      provide: LOCALE_ID,
      useValue: 'vi'
    },
    ModuleConfig,
    { provide: TranslateLanguageService, useClass: TranslateLanguageService }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule
    };
  }
}
