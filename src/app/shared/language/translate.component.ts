import { Component, Input } from '@angular/core';

/**
 * A wrapper directive on top of the translate pipe as the inbuilt translate directive from ngx-translate is too verbose and buggy
 */

/* tslint:disable */
@Component({
  selector: '[langTranslate]',
  template: '<span [innerHTML]="langTranslate | translate: translateValues"></span>'
})
export class TranslateComponent {

  @Input() langTranslate: string;

  @Input() translateValues: any;
}

/* tslint:enable */
