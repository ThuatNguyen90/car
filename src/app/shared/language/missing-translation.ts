import { MissingTranslationHandler, MissingTranslationHandlerParams } from '@ngx-translate/core';
import { ConfigService } from '../../core/services/config.service';

export class LangMissingTranslationHandler implements MissingTranslationHandler {
  constructor(private configService: ConfigService) {}

  handle(params: MissingTranslationHandlerParams) {
    const key = params.key;
    return `${this.configService.getConfig().noi18nMessage}[${key}]`;
  }
}
