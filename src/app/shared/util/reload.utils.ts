import { NavigationEnd, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class ReloadUtils {

  reloadComponent(router: Router): void {
    // override the route reuse strategy
    // tslint:disable-next-line:only-arrow-functions
    router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };

    router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        // trick the Router into believing it's last link wasn't previously loaded
        router.navigated = false;
        // if you need to scroll back to top, here is the right place
        window.scrollTo(0, 0);
      }
    });
  }
}
