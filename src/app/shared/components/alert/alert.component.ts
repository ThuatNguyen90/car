import { Component, OnDestroy, OnInit } from '@angular/core';
import {AlertService} from '../../../core/services';

@Component({
  selector: 'app-alert',
  template: `
    <div class="alerts" role="alert">
      <div *ngFor="let alert of alerts" [ngClass]="setClasses(alert)">
        <alert *ngIf="alert && alert.type && alert.msg" [type]="alert.type" (close)="alert.close(alerts)">
          <pre [innerHTML]="alert.msg"></pre>
        </alert>
      </div>
    </div>
  `
})
export class AlertComponent implements OnInit, OnDestroy {
  alerts: any[];

  constructor(private alertService: AlertService) {}

  ngOnInit() {
    this.alerts = this.alertService.get();
  }

  setClasses(alert) {
    return {
      'jhi-toast': alert.toast,
      [alert.position]: true
    };
  }

  ngOnDestroy() {
    this.alerts = [];
  }
}
