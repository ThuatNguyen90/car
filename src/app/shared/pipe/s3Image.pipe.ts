import { Injectable, Pipe, PipeTransform } from '@angular/core';

/*
 Transform image model to AWS S3 image.
 Use:
 {{ imageModel|s3Image[:imageSize]}}
 Ex:
 {{ imageModel|s3Image}} use default image
 {{ imageModel|s3Image: '100x100'}} resize image
 */
@Pipe({
  name: 's3Image'
})
@Injectable()
export class S3Image implements PipeTransform {
  transform(s3Image: any, args?: any): any {
    let s3ImageUrl = '';
    if (s3Image && s3Image.pathImage) {
      s3ImageUrl += s3Image.pathImage + '/';
      if (args && args.length > 0) {
        s3ImageUrl += args + '/';
      }
      if (s3Image.nameImage) {
        s3ImageUrl += s3Image.nameImage;
      }
    } else {
      s3ImageUrl = s3Image;
    }
    return s3ImageUrl;
  }
}
