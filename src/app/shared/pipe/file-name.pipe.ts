import { Pipe, PipeTransform } from '@angular/core';

/*
 * Usage:
 *   name | fileName:seperator
 * Example:
 *   {{ name |  fileName: '-'}}
 *   formats to: name file
*/
@Pipe({name: 'fileName'})
export class FileNamePipe implements PipeTransform {

  private units = [
    'bytes',
    'KB',
    'MB',
    'GB',
    'TB',
    'PB'
  ];

  transform(name: string, seperator: string ): string {
    let updateName = name;
    if (name.indexOf(seperator) > -1) {
      const nameArr: string[] = name.split(seperator);
      updateName = nameArr[1];
      if (nameArr.length > 2) {
        updateName = name.substr(nameArr[0].length + 1);
      }

    }
    return updateName;
  }
}
