export const DATE_FORMAT = 'DD-MM-YYYY';
export const DATE_TIME_FORMAT =  `${DATE_FORMAT} hh:mm:ss`;
export const DATE_TIME_NOT_SECOND_FORMAT =  `${DATE_FORMAT} hh:mm`;
export const MY_MOMENT_FORMATS = {
  parseInput: 'l LT',
  fullPickerInput: 'l LT',
  datePickerInput: 'l',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};
