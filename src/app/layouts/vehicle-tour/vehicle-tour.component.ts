import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'vehicle-tour',
  templateUrl: './vehicle-tour.component.html',
  styleUrls: ['./vehicle-tour.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class VehicleTourComponent {
  @Input() bgColor: string = 'red';
  @Input() keyVehicle: string = '50H - 9999';
  @Input() time: string = '06:00';
  @Input() numberBooked: number = 0;
  @Input() numberChair: number = 8;
  @Input() showXB: boolean = false;
  @Input() showTC: boolean = false;
  @Input() showBlock: boolean = false;
  public percentBooked = 0;
  constructor() { 
    this.percentBooked = this.numberBooked / this.numberChair; 
  }

}
