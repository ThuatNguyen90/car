import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'chair-info',
  templateUrl: './chair-info.component.html',
  styleUrls: ['./chair-info.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ChairInfoComponent {
  @Input() displayGilf: boolean = false;
  @Input() displayCard: boolean = false;
  @Input() displayWard: boolean = false;
  @Input() displayCircle: boolean = false;
  @Input() displayDollarHome: boolean = false;
  @Input() displayDollarBrand: boolean = false;
  @Input() displayPerson: boolean = false;
  @Input() displayMomo: boolean = false;
  @Input() bgColor: string = 'white';

  constructor() {  
  }
}
