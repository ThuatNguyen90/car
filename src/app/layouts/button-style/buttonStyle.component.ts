import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'button-style',
  templateUrl: './buttonStyle.component.html',
  styleUrls: ['./buttonStyle.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ButtonStyleComponent {
  @Input() colorName: string = 'red';
  constructor() { 
    
  }

}
