import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'checkbox-style',
  templateUrl: './checkboxStyle.component.html',
  styleUrls: ['./checkboxStyle.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CheckboxStyleComponent {
  constructor() { 
  }
}
