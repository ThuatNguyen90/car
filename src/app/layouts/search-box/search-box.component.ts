import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SearchBoxComponent {
  constructor() {  
  }
}
