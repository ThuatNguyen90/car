import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'radiobox-style',
  templateUrl: './radiobox-style.component.html',
  styleUrls: ['./radiobox-style.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RadioboxStyleComponent {
  constructor() { 
  }
}
