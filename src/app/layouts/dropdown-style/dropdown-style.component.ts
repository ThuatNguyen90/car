import { Component, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'dropdown-style',
  templateUrl: './dropdown-style.component.html',
  styleUrls: ['./dropdown-style.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DropdownStyleComponent {
  @Input() noBorderRadius: boolean = true;
  @Input() borderGreyColor: boolean = false;
  constructor() { 
    
  }

}
