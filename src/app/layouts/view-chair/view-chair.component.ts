import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'view-chair',
  templateUrl: './view-chair.component.html',
  styleUrls: ['./view-chair.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ViewChairComponent {
  info : ChairInfo;
  @Input() chairs: any[] = [];
  constructor() { 
    let info: ChairInfo ;
    for(let i = 0; i< 32 ; i++ ) {
      info.chairIndex = i;
      info.isBlocked = true;
      info.isAvailable = true;
      info.isBooked = true;
      this.chairs.push ({
        "key": i,
        "info": this.info
      })
    } 
  }
}
interface ChairInfo {
  chairIndex?: number;
  isBooked?: boolean;
  isAvailable?: boolean;
  isBlocked?: boolean;
}