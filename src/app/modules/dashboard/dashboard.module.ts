import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared';
import {DashboardRouteModule} from './dashboard.route.module';
import {DashboardComponent} from './pages/dashboard.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SearchBoxComponent } from 'src/app/layouts/search-box';
import { VehicleTourComponent } from 'src/app/layouts/vehicle-tour';
import { ChairComponent } from 'src/app/layouts/chair';
import { ButtonStyleComponent } from 'src/app/layouts/button-style';
import { ModalModule } from 'ngx-bootstrap';
import { DropdownStyleComponent } from 'src/app/layouts/dropdown-style';
import { RadioboxStyleComponent } from 'src/app/layouts/radiobox-style';
@NgModule({
  declarations: [
    DashboardComponent,
    SearchBoxComponent,
    VehicleTourComponent,
    ChairComponent,
    ButtonStyleComponent,
    DropdownStyleComponent,
    RadioboxStyleComponent
  ],
  imports: [
    SharedModule,
    DashboardRouteModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()

  ],
  exports: [],
  providers: [],
  entryComponents: []
})
export class DashboardModule {}
