import { NgModule } from '@angular/core';
import { LoginComponent } from './pages/login/login.component';
import { AuthRouteModule } from './auth.route.module';
import { SharedModule } from '../../shared';

@NgModule({
  imports: [
    SharedModule,
    AuthRouteModule
  ],
  declarations: [
    LoginComponent
  ]
})
export class AuthModule { }
