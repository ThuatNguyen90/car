import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared';
import {CheckTicketRouteModule} from './check-ticket.route.module';
import {CheckTicketComponent} from './pages/check-ticket.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SearchBoxComponent } from 'src/app/layouts/search-box';
import { PaginationModule } from 'ngx-bootstrap';
import { ChairInfoComponent } from 'src/app/layouts/chair-info';


@NgModule({
  declarations: [
    CheckTicketComponent,
    SearchBoxComponent,
    ChairInfoComponent
  ],
  imports: [
    SharedModule,
    CheckTicketRouteModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    PaginationModule.forRoot()
  ],
  exports: [],
  providers: [],
  entryComponents: []
})
export class CheckTicketModule {}
