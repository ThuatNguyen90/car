import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRouteAccessService } from '../../core';
import {CheckTicketComponent} from './pages/check-ticket.component';

export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CheckTicketComponent
      }
    ],
    // data: {
    //   authorities: ['ROLE_USER'],
    //   pageTitle: 'planManagerApp.dashboard.title'
    // },
    // canActivate: [UserRouteAccessService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckTicketRouteModule { }
