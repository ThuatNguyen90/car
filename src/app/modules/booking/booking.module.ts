import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared';
import {BookingRouteModule} from './booking.route.module';
import {BookingComponent} from './pages/booking.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SearchBoxComponent } from 'src/app/layouts/search-box';
import { TabsModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { PaginationModule } from 'ngx-bootstrap';


@NgModule({
  declarations: [
    BookingComponent,
    SearchBoxComponent
  ],
  imports: [
    SharedModule,
    BookingRouteModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    PaginationModule.forRoot()
  ],
  exports: [],
  providers: [],
  entryComponents: []
})
export class BookingModule {}
