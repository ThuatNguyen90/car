import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SharedModule } from '../../shared';
import { ActivateComponent } from './pages/activate/activate.component';
import { AccountRouteModule } from './account.route.module';
import { RegisterComponent } from './pages/register/register.component';
import { PasswordComponent } from './pages/password/password.component';
import { PasswordStrengthBarComponent } from './pages/password/password-strength-bar.component';
import { PasswordResetInitComponent } from './pages/password-reset/init/password-reset-init.component';
import { PasswordResetFinishComponent } from './pages/password-reset/finish/password-reset-finish.component';
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  imports: [
    SharedModule,
    AccountRouteModule,
    FileUploadModule
  ],
  declarations: [
    ActivateComponent,
    RegisterComponent,
    PasswordComponent,
    PasswordStrengthBarComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccountModule {}
