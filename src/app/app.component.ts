import {Component, OnInit} from '@angular/core';
import {ActivatedRouteSnapshot, NavigationEnd, NavigationError, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ConfigService} from './core/services/config.service';
import {LanguageHelper} from './shared/language/language.helper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  constructor(
    private translate: TranslateService,
    private configService: ConfigService,
    private languageHelper: LanguageHelper,
    private router: Router
  ) {
    const config = this.configService.getConfig();
    config.i18nEnabled = true;
    translate.setDefaultLang(this.configService.CONFIG_OPTIONS.defaultI18nLang);
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.languageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
      }
      if (event instanceof NavigationError && event.error.status === 404) {
        this.router.navigate(['/404']);
      }
    });
  }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
    let title = routeSnapshot.data && routeSnapshot.data.pageTitle ? routeSnapshot.data.pageTitle : 'global.title';
    if (routeSnapshot.firstChild) {
      title = this.getPageTitle(routeSnapshot.firstChild) || title;
    }

    return title;
  }
}
