import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {AuthInterceptor} from './core/interceptors/auth.interceptor';
import {NotificationInterceptor} from './core/interceptors/notification.interceptor';
import {AuthExpiredInterceptor} from './core/interceptors/auth-expired.interceptor';
import {ErrorHandlerInterceptor} from './core/interceptors/errorhandler.interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {MissingTranslationHandler, TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {missingTranslationHandler, SharedModule, translatePartialLoader} from './shared';
import {ConfigService} from './core/services/config.service';
import {CoreModule} from './core';
import {AccountModule} from './modules/account/account.module';
import {MainLayoutComponent} from './layouts/main-layout';
import {AuthLayoutComponent} from './layouts/auth-layout';
import {HeaderComponent} from './layouts/header';
import {FooterComponent} from './layouts/footer';
import {ErrorComponent} from './layouts/error';
import {NgxSpinnerModule} from 'ngx-spinner';
import { BsDropdownModule } from 'ngx-bootstrap';

const APP_CONTAINERS = [
  MainLayoutComponent,
  AuthLayoutComponent
];

const APP_COMPONENTS = [
  FooterComponent,
  HeaderComponent,
  ErrorComponent
];

@NgModule({
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    ...APP_COMPONENTS
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    AppRoutingModule,
    BrowserModule,
    NgxSpinnerModule,
    NgxWebstorageModule.forRoot({ prefix: 'starbap', separator: '-' }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translatePartialLoader,
        deps: [HttpClient]
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useFactory: missingTranslationHandler,
        deps: [ConfigService]
      },
      useDefaultLang: true
    }),
    SharedModule.forRoot(),
    CoreModule,
    AccountModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotificationInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
