import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class TranslateLanguageService {
  currentLang = 'vi';

  constructor(private translateService: TranslateService, private configService: ConfigService) {
    this.init();
  }

  init() {
    const config = this.configService.getConfig();
    this.currentLang = config.defaultI18nLang;
    this.translateService.setDefaultLang(this.currentLang);
    this.translateService.use(this.currentLang);
  }

  changeLanguage(languageKey: string): Observable<any> {
    if (languageKey) {
      this.currentLang = languageKey;
      this.configService.CONFIG_OPTIONS.defaultI18nLang = languageKey;
    }
    return this.translateService.use(this.currentLang);
  }

  // tslint:disable-next-line:ban-types
  instant(key: string | Array<string>, interpolateParams?: Object): string | any {
    return this.translateService.instant(key, interpolateParams);
  }

  getCurrent(): Promise<string> {
    return Promise.resolve(this.currentLang);
  }

}
